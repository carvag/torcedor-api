# API Torcedor

Códigos desenvolvidos para prover serviços rest para incluir Torcedor e chamada de service API Campanha para realação com torcedor

Instruções específicas:
## TorcedorAPI
Efetuar o build com o Maven (mvn clean package spring-boot:run) e após isso executr com PostMan, curl, etc.

## Banco H2
```sh
http://localhost:8090/console

```

## Endpoint
```sh
http://localhost:8090/api/torcedor
```
#### Create

```sh
curl -H "Accept: application/json" -H "Content-Type: application/json"  -X POST -d '{"name":"Gustavo", "email":"gustavo@gustavo.br", "team": 1, "birthday": "18/06/1993"}' http://localhost:8090/api/torcedor/create
```

####Times
```sh
1 vasco
2 Flamengo
3 Fluminense
4 Botafogo
```

## Tecnologias Usadas

 - Java versão 8.
 - Feign: Cliente HTTP via JSON para chamar REST Services
 - Maven: ferramenta de automação de compilação
 - JPA / Hibernate: mapeamento de entidades persistentes em pojos de domínio, resolvendo o ORM, abstraindo a escrita de código SQL (DDL e DML).
 - H2: Banco de Dados embutido, já com SQL iniciado
 - Bean Validations: framework para definição de regras de validação em entidades JPA via anotações.
 - Logback: geração de logs.
 - Spring Data JPA: tecnologia responsável por gerar boa parte do código relacionado a camada de persistência. No nível da aplicação eu escrevo os contratos de persistência, que funcionam como ponto de partida para a criação dos comandos de manipulação (CRUD), consultas simples e sofisticadas.
 - Spring Web MVC: framework web usado como solução MVC e para a definição de componentes seguindo o modelo arquitetural REST.
 - Jackson: API para conversão de dados Java em Json e vice-versa.
 - Postman

License
----
GNU PUBLIC LICENSE


----------


**Gustavo Carvalho**
