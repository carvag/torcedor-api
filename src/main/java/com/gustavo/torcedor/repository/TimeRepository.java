package com.gustavo.torcedor.repository;

import com.gustavo.torcedor.model.Time;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TimeRepository extends JpaRepository<Time, Long> {
}
