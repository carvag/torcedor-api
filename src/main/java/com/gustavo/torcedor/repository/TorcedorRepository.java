package com.gustavo.torcedor.repository;

import com.gustavo.torcedor.model.Time;
import com.gustavo.torcedor.model.Torcedor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TorcedorRepository extends JpaRepository<Torcedor, Long> {

    Optional<Torcedor> findByEmail(String email);
}
