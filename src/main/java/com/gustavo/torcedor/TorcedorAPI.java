package com.gustavo.torcedor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TorcedorAPI {
	public static void main(String[] args) throws Exception {
		SpringApplication.run(TorcedorAPI.class, args);
	}
}
