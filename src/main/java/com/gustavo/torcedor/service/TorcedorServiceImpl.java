package com.gustavo.torcedor.service;

import com.gustavo.torcedor.controller.APIController;
import com.gustavo.torcedor.model.Torcedor;
import com.gustavo.torcedor.repository.TorcedorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service("torcedorService")
@Transactional
public class TorcedorServiceImpl implements TorcedorService{

    @Autowired
    private TorcedorRepository torcedorRepository;

    public static final Logger logger = LoggerFactory.getLogger(APIController.class);

    public Torcedor findById(Long id) {
        return torcedorRepository.findOne(id);
    }

    public Optional findByEmail(String email){ return torcedorRepository.findByEmail(email);}

    public List<Torcedor> findAll() {
        return torcedorRepository.findAll();
    }

    public Torcedor saveTorcedor(Torcedor torcedor) {
        return torcedorRepository.save(torcedor);
    }
}
