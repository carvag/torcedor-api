package com.gustavo.torcedor.service;


import com.gustavo.torcedor.model.Campanha;
import com.gustavo.torcedor.model.Torcedor;
import feign.Headers;
import feign.RequestLine;
import org.springframework.web.bind.annotation.PathVariable;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.List;

public interface CampanhaService {

    @Headers("Content-Type: application/json")
    @RequestLine("POST /campanha/torcedor")
    List<Campanha> registerClienteCampanha(@PathVariable("torcedor") Torcedor torcedor);

    static Boolean pingHost() {
        try (Socket socket = new Socket()) {
            socket.connect(new InetSocketAddress("localhost", 8080), 3);
            return true;
        } catch (IOException e) {
            return false;
        }
    }
}
