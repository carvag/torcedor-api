package com.gustavo.torcedor.service;

import com.gustavo.torcedor.model.Torcedor;


import java.util.List;
import java.util.Optional;

public interface TorcedorService {

    Torcedor findById(Long id);

    public Optional findByEmail(String email);

    List<Torcedor> findAll();

    Torcedor saveTorcedor(Torcedor torcedor);

}
