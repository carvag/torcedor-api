package com.gustavo.torcedor.service;


import com.gustavo.torcedor.model.Time;
import com.gustavo.torcedor.repository.TimeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service("timeService")
@Transactional
public class TimeServiceImpl implements TimeService{

    @Autowired
    private TimeRepository timeRepository;

    @Override
    public Time findById(Long id) {
        return timeRepository.findOne(id);
    }

    @Override
    public List<Time> findAllTimes() {
        return timeRepository.findAll();
    }
}
