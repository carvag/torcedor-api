package com.gustavo.torcedor.service;

import com.gustavo.torcedor.model.Time;

import java.util.List;

public interface TimeService {

    Time findById(Long id);

    List<Time> findAllTimes();
}
