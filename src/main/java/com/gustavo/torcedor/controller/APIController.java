package com.gustavo.torcedor.controller;

import com.gustavo.torcedor.model.Campanha;
import com.gustavo.torcedor.model.Torcedor;

import com.gustavo.torcedor.service.CampanhaService;
import com.gustavo.torcedor.service.TimeService;
import com.gustavo.torcedor.util.CustomMessage;
import com.gustavo.torcedor.service.TorcedorService;
import feign.Feign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@EnableWebMvc
@RestController
@RequestMapping("/api/torcedor")
public class APIController {

    public static final Logger logger = LoggerFactory.getLogger(APIController.class);

    @Autowired
    TorcedorService torcedorService;

    @Autowired
    TimeService timeService;


    // -------------------List Torcedores---------------------------------------------
    @RequestMapping(method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<List<Torcedor>> listAllTorcedores(){
        List<Torcedor> campanhas = torcedorService.findAll();
        if(campanhas.isEmpty()){
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Torcedor>>(campanhas, HttpStatus.OK);
    }

    // -------------------Get Campanha by Id---------------------------------------------
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> getTorcedor(@PathVariable("id") long id){
        logger.info("Getting by Id {}", id);
        Torcedor torcedor = torcedorService.findById(id);
        if(torcedor == null){
            logger.info("Torcedor id {} not found", id);
            return new ResponseEntity(new CustomMessage("Torcedor ID "+ id + "not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Torcedor>(torcedor, HttpStatus.OK);
    }

    // -------------------Register Torcedor---------------------------------------------
    @RequestMapping(value = "create", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> registerTorcedor(@RequestBody @Valid Torcedor torcedor) throws IllegalArgumentException{
        logger.info("torcedor {}", torcedor);
        String type = "na base";

        Optional torcedorOptional = torcedorService.findByEmail(torcedor.getEmail());
        if(!torcedorOptional.isPresent()){
            type = "criado";
            logger.info("Registering Torcedor {} ", torcedor.getEmail());
            torcedorService.saveTorcedor(torcedor);
            //return new ResponseEntity(new CustomMessage("Torcedor "+ torcedor.getEmail() + "jah cadastrado"), HttpStatus.FOUND);
        }

        if(CampanhaService.pingHost() == true){
            final String URI_API = "http://localhost:8080/api/";
            CampanhaService campanhaService = Feign.builder().encoder(new JacksonEncoder())
                    .decoder(new JacksonDecoder()).target(CampanhaService.class, URI_API);
            List<Campanha> campanhas = campanhaService.registerClienteCampanha(torcedor);

            if(campanhas.isEmpty()){
                return new ResponseEntity<>(new CustomMessage("Não foi econtrado campanha para este time!"),HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(campanhas, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(new CustomMessage("Torcedor "+ type +", porem servicos de campanha esta fora!"), HttpStatus.CREATED);
    }

}
