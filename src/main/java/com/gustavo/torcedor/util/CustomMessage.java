package com.gustavo.torcedor.util;

public class CustomMessage {

    private String message;

    public CustomMessage(String message){
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
