package com.gustavo.torcedor;

import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.gustavo.torcedor.controller.APIController;
import com.gustavo.torcedor.model.Time;
import com.gustavo.torcedor.model.Torcedor;
import com.gustavo.torcedor.service.TimeService;
import com.gustavo.torcedor.service.TorcedorService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.web.context.WebApplicationContext;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.nio.charset.Charset;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TorcedorAPITests{

    private MockMvc mockMvc;

    @Autowired
    WebApplicationContext context;

    @Autowired
    ObjectMapper objectMapper;

    @Mock
    TorcedorService torcedorService;

    @Mock
    TimeService timeService;

    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    @InjectMocks
    APIController apiController;

    @Before
    public void setUp(){
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
    }



    @Test
    public void getListTorcedores() throws Exception{
        this.mockMvc.perform(get("/api/torcedor").accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getTorcedorById() throws Exception{
        final Long id = 1l;
        this.mockMvc.perform(get(String.format("/api/torcedor/%s", id)).accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void createTorcedor() throws Exception{

        MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", java.nio.charset.Charset.forName("UTF-8"));

        String torcedorJson = createTorcedorInJson("gustavo","gu@gu.com","18/06/1993");

        this.mockMvc.perform(post("/api/torcedor/create")
                .content(torcedorJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    private static String createTorcedorInJson (String name, String email, String birthday) {
        return "{ \"name\": \"" + name + "\", " +
                "\"email\":\"" + email + "\"," +
                "\"team\":1, "+
                "\"birthday\":\"" + birthday + "\"}";
    }
}
